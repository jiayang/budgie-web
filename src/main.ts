import Vue from "vue";
import * as firebase from "firebase/app";
import App from "@/App.vue";
import router from "@/router";
import store from "@/store";
import "@/registerServiceWorker";
import {} from "googlemaps";
import { FIREBASE_CONFIG } from "@/shared/global-values";

firebase.initializeApp(FIREBASE_CONFIG);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
