import Vue from "vue";
import Router, { Route, RouteRecord } from "vue-router";
import * as firebase from "firebase/app";
import "firebase/auth";
import Home from "@/views/Home.vue";
import GetStarted from "@/views/GetStarted.vue";
import Account from "@/views/Account.vue";
import ChangeEmail from "@/views/ChangeEmail.vue";
import NewExpense from "@/views/NewExpense.vue";
import Expense from "@/views/Expense.vue";
import EditExpense from "@/views/EditExpense.vue";
import Search from "@/views/Search.vue";

Vue.use(Router);

const router: Router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    { path: "/", name: "Home", component: Home, meta: { requiresAuth: true } },
    { path: "/get-started", name: "GetStarted", component: GetStarted },
    { path: "/account", name: "Account", component: Account, meta: { requiresAuth: true } },
    { path: "/change-email", name: "ChangeEmail", component: ChangeEmail, meta: { requiresAuth: true } },
    { path: "/expense/new", name: "NewExpense", component: NewExpense, meta: { requiresAuth: true } },
    { path: "/expense/:id", name: "Expense", component: Expense, meta: { requiresAuth: true } },
    { path: "/expense/:id/edit", name: "EditExpense", component: EditExpense, meta: { requiresAuth: true } },
    { path: "/search", name: "Search", component: Search, meta: { requiresAuth: true } },
  ],
});

router.beforeEach(async (to: Route, from: Route, next: Function) => {
  const user: firebase.User | null = firebase.auth().currentUser;
  if (to.matched.some((record: RouteRecord) => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!user) {
      return next({
        name: "GetStarted",
        query: { redirect: to.fullPath },
      });
    }
  } else if (to.matched.some((record: RouteRecord) => ["GetStarted"].includes(record.name!))) {
    if (user) {
      return next({
        name: "Home",
        query: { redirect: to.fullPath },
      });
    }
  }

  next(); // make sure to always call next()!
});

export default router;
