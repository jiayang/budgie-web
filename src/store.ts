import Vue from "vue";
import Vuex from "vuex";
import IExpense from "@/types/expense";
import IFilter from "@/types/filter";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    filters: { dateFrom: null, dateTo: null, colours: new Set<string>([]) } as IFilter,
    expenses: [] as IExpense[],
    isFilterVisible: false,
    isLocationSearchVisible: false,
    isFiltered: false,
  },
  mutations: {
    setFilters(state, payload): void {
      state.filters = { ...state.filters, ...payload };
    },
    setExpenses(state, payload): void {
      state.expenses = [...payload];
    },
    setIsFilterVisible(state, payload): void {
      state.isFilterVisible = payload;
    },
    setIsLocationSearchVisible(state, payload): void {
      state.isLocationSearchVisible = payload;
    },
    setIsFiltered(state, payload): void {
      state.isFiltered = payload;
    },
  },
  actions: {
    setFilters(context, payload): void {
      context.commit("setFilters", payload);
    },
    setExpenses(context, payload): void {
      context.commit("setExpenses", payload);
    },
    toggleFilter(context, payload): void {
      context.commit("setIsFilterVisible", !context.state.isFilterVisible);
    },
    toggleLocationSearch(context, payload): void {
      context.commit("setIsLocationSearchVisible", !context.state.isLocationSearchVisible);
    },
    setIsFiltered(context, payload): void {
      context.commit("setIsFiltered", payload);
    },
  },
  getters: {
    getFilters(state, getters): IFilter {
      return state.filters;
    },
    getExpenses(state, getters): IExpense[] {
      return state.expenses;
    },
    getIsFilterVisible(state, getters): boolean {
      return state.isFilterVisible;
    },
    getIsLocationSearchVisible(state, getters): boolean {
      return state.isLocationSearchVisible;
    },
    getIsFiltered(state, getters): boolean {
      return state.isFiltered;
    },
  },
});
