interface Number {
  toCurrency(): string;
}

Number.prototype.toCurrency = function(): string {
  return this.toFixed(2);
};
