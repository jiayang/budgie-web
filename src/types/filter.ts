export default interface IFilter {
  dateFrom: Date | null;
  dateTo: Date | null;
  colours: Set<string>;
}
