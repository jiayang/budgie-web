export default interface IUser {
  username: string;
  budget: number;
  budgetPeriod: string;
  hasBudgetRollover: boolean;
  budgetRolloverFrom: Date;
  createdAt: Date;
}
