export default interface ILocation {
  name: string;
  address: string;
  latitude: number;
  longitude: number;
}
