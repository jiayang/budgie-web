import ILocation from "@/types/location";

export default interface IExpense {
  id: number;
  userId: number;
  name: string;
  amount: number;
  date: Date | string;
  colour: string;
  notes: string;
  photoURL: string;
  createdAt: Date;
  location: ILocation;

  // Not part of schema.
  colourHex: string;
}
