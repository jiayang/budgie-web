import moment, { Moment } from "moment";
import IExpense from "@/types/expense";

export function formatDate(date: Date, format: string): string {
  const mToday: Moment = moment();
  const yesterday: Moment = moment().subtract(1, "day");
  const mDate: Moment = moment(date);

  if (mDate.isSame(mToday, "day")) {
    return "Today";
  } else if (mDate.isSame(yesterday, "day")) {
    return "Yesterday";
  }

  let formattedDate: string = mDate.format(format);

  if (mDate.isSame(mToday, "year")) {
    formattedDate = mDate.format(format.replace(/Y|y/g, "").trim());
  }

  return formattedDate;
}

export function getDateRangeOfBudgetPeriod(date: Date, budgetPeriod: string): { dateFrom: Date, dateTo: Date } {
  switch (budgetPeriod) {
    case "DAY": {
      const dateFrom: Date = moment(date).startOf("day").toDate();
      const dateTo: Date = moment(date).endOf("day").toDate();

      return { dateFrom, dateTo };
    }
    case "WEEK": {
      const dateFrom: Date = moment(date).startOf("isoWeek").toDate();
      const dateTo: Date = moment(date).endOf("isoWeek").toDate();

      return { dateFrom, dateTo };
    }
    case "MONTH": {
      const dateFrom: Date = moment(date).startOf("month").toDate();
      const dateTo: Date = moment(date).endOf("month").toDate();

      return { dateFrom, dateTo };
    }
    case "YEAR": {
      const dateFrom: Date = moment(date).startOf("year").toDate();
      const dateTo: Date = moment(date).endOf("year").toDate();

      return { dateFrom, dateTo };
    }
    default:
      throw new Error(`Budget period "${budgetPeriod}" is not supported.`);
  }
}

export function groupExpensesByDate(expenses: IExpense[]): { [date: string]: IExpense[] } {
  return expenses.reduce((acc: { [date: string]: IExpense[] }, expense: IExpense) => {

    const date: string = formatDate(new Date(expense.date), "D MMM YY");

    if (!acc[date]) {
      acc[date] = [];
    }

    acc[date].push(expense);

    return acc;

  }, {});
}

export function countExpensesByColours(expenses: IExpense[]): { [colourHex: string]: number } {
  const colourHexToCount: { [colourHex: string]: number } = {};

  for (const expense of expenses) {
    if (!colourHexToCount[expense.colourHex]) {
      colourHexToCount[expense.colourHex] = 0;
    }

    colourHexToCount[expense.colourHex]++;
  }

  return Object.entries(colourHexToCount).sort((a, b) => {
    if (a[1] > b[1]) {
      return -1;
    }
    if (a[1] < b[1]) {
      return 1;
    }
    return 0;
  }).reduce((acc: { [colourHex: string]: number }, entries: any[]) => {

    const [colourHex, count] = entries;
    acc[colourHex] = count;

    return acc;

  }, {});
}

export function searchPlace(query: string): Promise<google.maps.places.PlaceResult[]> {
  return new Promise((resolve, reject) => {
    const request = {
      query,
      fields: ["formatted_address", "geometry", "name"],
    };

    const service = new google.maps.places.PlacesService(document.createElement("div"));

    service.findPlaceFromQuery(request, (results: google.maps.places.PlaceResult[], status: google.maps.places.PlacesServiceStatus) => {
      if (status === google.maps.places.PlacesServiceStatus.OK) {
        resolve(results);
      } else {
        resolve([]);
      }
    });
  });
}
