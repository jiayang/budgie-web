export const COLOUR_OPTIONS: Array<{ colour: string, hex: string }> = [
  { colour: "RED", hex: "#b7472a" },
  { colour: "GREEN", hex: "#217346" },
  { colour: "BLUE", hex: "#2b579a" },
  { colour: "PURPLE", hex: "#7719aa" },
];
export const BUDGET_PERIOD_OPTIONS: string[] = ["DAY", "WEEK", "MONTH", "YEAR"];

export const MAPS_CREDENTIALS: { API_KEY: string, PLACE_SEARCH_API: string, STATIC_IMAGE_API: string } = {
  API_KEY: process.env.VUE_APP_GOOGLE_API_KEY as string,
  PLACE_SEARCH_API: "https://maps.googleapis.com/maps/api/place/findplacefromtext/json",
  STATIC_IMAGE_API: "https://maps.googleapis.com/maps/api/staticmap",
};

export const FIREBASE_CONFIG: { [key: string]: string } = {
  apiKey: process.env.VUE_APP_FIREBASE_API_KEY as string,
  authDomain: "budgie-1624.firebaseapp.com",
  databaseURL: "https://budgie-1624.firebaseio.com",
  projectId: "budgie-1624",
  storageBucket: "budgie-1624.appspot.com",
  messagingSenderId: process.env.VUE_APP_FIREBASE_MESSAGING_SENDER_ID as string,
  appId: process.env.VUE_APP_FIREBASE_APP_ID as string,
};

export const AWS_CREDENTIALS: { ACCESS_KEY_ID: string, SECRET_ACCESS_KEY: string } = {
  ACCESS_KEY_ID: process.env.VUE_APP_AWS_ACCESS_KEY_ID as string,
  SECRET_ACCESS_KEY: process.env.VUE_APP_AWS_SECRET_ACCESS_KEY as string,
};

export const DATE: { YEARS: number[], MONTHS: string[], DAYS: number[] } = {
  YEARS: (() => {
    const years: number[] = [];
    for (let i = 1970; i <= new Date().getFullYear(); i++) {
      years.push(i);
    }
    return years;
  })(),
  MONTHS: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
  DAYS: (() => {
    const days: number[] = [];
    for (let i = 1; i <= 31; i++) {
      days.push(i);
    }
    return days;
  })(),
};

export const API_BASE_URL: string = process.env.VUE_APP_API_BASE_URL as string;
export const S3_BUCKET: string = process.env.VUE_APP_S3_BUCKET as string;
