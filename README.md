Frontend of the [Budgie](https://laijiayang.com/development) web app.

### Stack
- Vue.js
- .Net Core
- AWS S3
- AWS SES
- AWS Lightsail
- Docker
- PostgreSQL
- Redis
- Firebase Authentication
- Firebase Hosting
- Gitlab CI

### Other repositories
- [Backend](https://gitlab.com/jiayang/budgie)
